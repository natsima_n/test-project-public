FROM ubuntu:16.04

# Install apache, PHP, and supplimentary programs. openssh-server, curl, and lynx-cur are for debugging the container.
RUN sed -i 's/\/us.archive/\/th.archive/g' /etc/apt/sources.list
RUN sed -i 's/\/archive/\/th.archive/g' /etc/apt/sources.list

RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install openjdk-8-jdk \
    apache2 php7.0 php7.0-mysql libapache2-mod-php7.0 curl lynx-cur \
    php7.0-gd php-imagick php7.0-imap php7.0-intl php7.0-mbstring php7.0-mcrypt php-memcached php7.0-sqlite3 \
    php7.0-pspell php7.0-recode php7.0-xml  php7.0-xmlreader  php7.0-xmlrpc  php7.0-xmlwriter php7.0-tidy php7.0-xsl \
    php-redis

# Enable apache mods.
RUN a2enmod php7.0
RUN a2enmod rewrite
RUN a2enmod ssl
RUN ulimit -n 500000


# Add apache2 configuration
ADD apache2.conf /etc/apache2/apache2.conf
# Add PHP configuration
ADD php.ini /etc/php/7.0/apache2/php.ini

#file ulimit
ADD limits.conf	/etc/security/limits.conf
ADD mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

# Manually set up the apache environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid


#Set Time Zone BKK
RUN apt-get install -y tzdata && \
 ln -sf /usr/share/zoneinfo/Asia/Bangkok /etc/localtime && \
 dpkg-reconfigure -f noninteractive tzdata

# Prepare directory
RUN mkdir -p /var/www
RUN mkdir -p /share

# Expose apache.
EXPOSE 80
EXPOSE 443

CMD /usr/sbin/apache2ctl -D FOREGROUND
